package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import appium.FooterOptions;
import appiumUtils.Common;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SignInPage {

	public SignInPage(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id = "com.eventyay.attendee:id/email")
	public WebElement emailTextBox;

	@AndroidFindBy(uiAutomator = "text(\"Get Started\")")
	public WebElement buttonGetStarted;

	@AndroidFindBy(id = "com.eventyay.attendee:id/password")
	public WebElement passwordTextBox;

	@AndroidFindBy(id = "com.eventyay.attendee:id/loginButton")
	public WebElement buttonLogin;
	
	@AndroidFindBy(id = "com.eventyay.attendee:id/setting")
	public WebElement settingIcon;

	/**
	 * 
	 * @param email
	 * @param password
	 */
	public void userLogin(String email, String password, AppiumDriver driver) {
		EventsPage ent = new EventsPage(driver);
		if (ent.eventIcon.isDisplayed()) {
			FooterOptions footerOptions = new FooterOptions(driver);
			footerOptions.getProfileIcon().click();
			Common.waitforPageToLoad(driver, 20);
			ProfilePage profilePage = new ProfilePage(driver);
			profilePage.linkLogin.click();
		}
		emailTextBox.click();
		emailTextBox.sendKeys(email);
		buttonGetStarted.click();
		passwordTextBox.click();
		passwordTextBox.sendKeys(password);
		buttonLogin.click();
	}
}
