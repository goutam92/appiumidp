package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class EventsPage {

	public EventsPage(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id = "com.eventyay.attendee:id/locationTextView")
	public WebElement locationName;

	@AndroidFindBy(id = "com.eventyay.attendee:id/whatIsGoodText")
	public WebElement globalText;

	@AndroidFindBy(xpath = "//*[@resource-id='com.eventyay.attendee:id/eventIcon']")
	public WebElement eventIcon;

	@FindBy(xpath = "//h2[@class='main-heading']")
	public WebElement eventPageHeader;

}
