package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ProfilePage {

	public ProfilePage(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(uiAutomator = "text(\"Profile\")")
	public WebElement headerProfile;

	@AndroidFindBy(uiAutomator = "text(\"Log In\")")
	public WebElement linkLogin;

	@AndroidFindBy(id = "com.eventyay.attendee:id/accountEmail")
	public WebElement userEmailID;

	@AndroidFindBy(id = "com.eventyay.attendee:id/accountName")
	public WebElement userName;
	
	@AndroidFindBy(id = "com.eventyay.attendee:id/logout")
	public WebElement linkLogout;
	
	@AndroidFindBy(uiAutomator = "text(\"LOG OUT\")")
	public WebElement logoutOption;

	
}
