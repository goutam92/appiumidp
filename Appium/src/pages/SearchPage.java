package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import appium.FooterOptions;
import appiumUtils.Common;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SearchPage {

	public SearchPage(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id = "com.eventyay.attendee:id/eventTypeTextView")
	public WebElement eventType;
	
	@AndroidFindBy(id = "com.eventyay.attendee:id/typeTextView")
	public List<WebElement> eventList;
	
	@AndroidFindBy(id = "com.eventyay.attendee:id/fabSearch")
	public AndroidElement searchArrow;
	
	@AndroidFindBy(id = "com.eventyay.attendee:id/filter")
	public AndroidElement filterIcon;
	
	@AndroidFindBy(id = "com.eventyay.attendee:id/freeStuffCheckBox")
	public AndroidElement freeStuffCheckBox;
	
	@AndroidFindBy(id = "com.eventyay.attendee:id/dateRadioButton")
	public AndroidElement dateRadioButton;

}
