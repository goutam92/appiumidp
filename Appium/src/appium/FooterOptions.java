package appium;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FooterOptions {

	public FooterOptions(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id = "com.eventyay.attendee:id/eventsFragment")
	public WebElement eventsIcon;

	@AndroidFindBy(id = "com.eventyay.attendee:id/searchFragment")
	public WebElement searchIcon;
	
	@AndroidFindBy(id = "com.eventyay.attendee:id/favoriteFragment")
	public WebElement favIcon;
	
	@AndroidFindBy(id = "com.eventyay.attendee:id/orderUnderUserFragment")
	public WebElement userOrderIcon;
	
	@AndroidFindBy(id = "com.eventyay.attendee:id/profileFragment")
	public WebElement profileIcon;

	public WebElement getEventsIcon() {
		return eventsIcon;
	}

	public WebElement getSearchIcon() {
		return searchIcon;
	}

	public WebElement getFavIcon() {
		return favIcon;
	}

	public WebElement getUserOrderIcon() {
		return userOrderIcon;
	}

	public WebElement getProfileIcon() {
		return profileIcon;
	}
	
	
}
