package appium;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class Base {
	public static DesiredCapabilities Capabalities() throws Exception {
		// Step-1 Start appium server
		// Step-2 Open emulator
		// Step-3 set cap and send those capabilities from your client to appium server
		File file = new File("app");
		File path = new File(file, "open-event-debug.apk");
		// Set Capabilities.
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "emulatorTest");
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
		cap.setCapability(MobileCapabilityType.APP, path.getAbsolutePath());
		cap.setCapability(MobileCapabilityType.NO_RESET, "true");
		return cap;
		
		//app package - com.eventyay.attendee/
		//app activity - org.fossasia.openevent.general.MainActivity
		// connection to server

		// AndroidUIautomator tool used to find elemnt from dom
		// Path - C:\Users\goutam.patra\AppData\Local\Android\sdk\tools\bin

	}

	public static AndroidDriver<AndroidElement> getDriver() throws Exception {
		DesiredCapabilities cap = Base.Capabalities();
		AndroidDriver<AndroidElement> driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return driver;
	}

	// @AfterMethod
	public void deleteCookies(AppiumDriver driver) {
		driver.manage().deleteAllCookies();
	}

}
