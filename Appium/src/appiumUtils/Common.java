package appiumUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;

public class Common {

	public static boolean isElementDisplayed(MobileElement element) {
		try {
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Set implicit wait in seconds *
	 */
	public static void waitforPageToLoad(AppiumDriver driver, int seconds) {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}

	/**
	 * @This method get data from file using file path and data key.
	 */
	public static String getValue(String path, String key) {
		try (InputStream input = new FileInputStream(path)) {
			Properties prop = new Properties();
			// load a properties file
			prop.load(input);
			// get the property value and print it out
			return prop.getProperty(key);
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * Scroll to view
	 */

	// driver.findElementByAndroidUIAutomator("new UiScrollable(new
	// UiSelector()).scrollIntoView(text(\"WebView\"));");

	public static void swipeToElement(AndroidDriver driver, String visibleText) {
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""
						+ visibleText + "\").instance(0))");
	}

	/**
	 * SWitch to alert
	 */
	public static boolean isAlertPresent(AppiumDriver driver) {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException Ex) {
			return false;
		}
	}

	/**
	 * SWitch to context
	 * 
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unchecked")
	public static void switchToContext(AppiumDriver driver) throws InterruptedException {
		Set<String> contexts = driver.getContextHandles();
		Object[] arry = contexts.toArray();
		if (contexts.size() > 3) {
			try {
				driver.context((String) arry[1]);
			} catch (Exception e) {
				driver.context((String) arry[2]);
			}
		} else {
			driver.context((String) arry[1]);
		}
	}

	public static void waitForElement(AppiumDriver driver, By freeStuffCheckBox) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(freeStuffCheckBox));

	}

	/**
	 * SWitch to native view context
	 * 
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unchecked")
	public static void switchToNativeContext(AppiumDriver driver) {
		Set<String> contexts = driver.getContextHandles();
		for (String contextName : contexts) {
			if (contextName.contains("NATIVE")) {
				driver.context(contextName);
			}
		}
	}

	public static void swipeAndClick(AndroidDriver driver, String visibleText) {
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""
						+ visibleText + "\").instance(0))")
				.click();
	}

	public static void tapOnElement(AndroidDriver driver, AndroidElement ele) {
		TouchAction touch = new TouchAction(driver);
		touch.tap(tapOptions().withElement(element(ele))).perform();
	}
}
