package suit;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import appium.Base;
import appium.FooterOptions;
import appiumUtils.Common;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.clipboard.ClipboardContentType;
import listener.TestListener;
import pages.EventsPage;
import pages.ProfilePage;
import pages.SearchPage;
import pages.SettingPage;
import pages.SignInPage;

@Listeners(TestListener.class)
public class Developement extends Base {

	static String filePath = "resources/config.properties";
	static String emailID = Common.getValue(filePath, "email");
	static String passWord = Common.getValue(filePath, "password");

	@Test(description = "Verify elements in Events page", enabled = false)
	public static void Test_01() throws Exception {
		AndroidDriver<AndroidElement> driver = getDriver();
		EventsPage eventsPage = new EventsPage(driver);
		String locationName = eventsPage.locationName.getText();
		Assert.assertEquals(!(locationName.isEmpty()), true);
		System.out.println("Actual location name is " + locationName);
		Assert.assertEquals(eventsPage.globalText.getText(), "What's good in", "Strings are not equal");
		Assert.assertEquals(eventsPage.eventIcon.isDisplayed(), true, "Event icon is displayed on Event page");
	}

	@Test(description = "Navigate to Profile page, verify details and login succesfully", enabled = false)
	public static void Test_02() throws Exception {
		AndroidDriver<AndroidElement> driver = getDriver();
		FooterOptions footerOptions = new FooterOptions(driver);
		footerOptions.getProfileIcon().click();
		Common.waitforPageToLoad(driver, 20);
		ProfilePage profilePage = new ProfilePage(driver);
		Assert.assertEquals(profilePage.headerProfile.getText(), "Profile", "Header text is not Profile");
		Assert.assertEquals(profilePage.linkLogin.getText(), "Log In", "Log In link is not present");

		// user login
		profilePage.linkLogin.click();
		Common.waitforPageToLoad(driver, 20);
		SignInPage signInPage = new SignInPage(driver);
		signInPage.userLogin(emailID, passWord, driver);
		Common.waitforPageToLoad(driver, 20);
		Assert.assertEquals(profilePage.userEmailID.getText(), emailID, "User is not logged in successfully");
	}

	@Test(description = "User log out successfully", enabled = false)
	public static void Test_03() throws Exception {
		AndroidDriver<AndroidElement> driver = getDriver();
		SignInPage signInPage = new SignInPage(driver);
		signInPage.userLogin(emailID, passWord, driver);
		Common.waitforPageToLoad(driver, 20);
		Common.swipeToElement(driver, "Log out");
		ProfilePage profilePage = new ProfilePage(driver);
		profilePage.linkLogout.click();
		profilePage.logoutOption.click();
		EventsPage eventsPage = new EventsPage(driver);
		Assert.assertEquals(eventsPage.eventIcon.isDisplayed(), true, "User is logut successfully");
	}

	@Test(description = "Context change scenario and verification", enabled = false)
	public static void Test_04() throws Exception {
		AndroidDriver<AndroidElement> driver = getDriver();
		FooterOptions footerOptions = new FooterOptions(driver);
		footerOptions.getFavIcon().click();
		Common.waitforPageToLoad(driver, 20);
		SignInPage signInPage = new SignInPage(driver);
		signInPage.settingIcon.click();
		SettingPage settingPage1 = new SettingPage(driver);
		settingPage1.visitWebsite.click();
		Common.waitforPageToLoad(driver, 40);
		// navigate to web view context
		Common.switchToContext(driver);
		EventsPage event = new EventsPage(driver);
		Assert.assertEquals(event.eventPageHeader.isDisplayed(), true, "Header is not present on event page");
		System.out.println("Event page header is : " + event.eventPageHeader.getText());

		// navigate back to native view context
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		Common.waitforPageToLoad(driver, 20);
		Common.switchToNativeContext(driver);
		SettingPage settingPage2 = new SettingPage(driver);
		Assert.assertEquals(settingPage2.visitWebsite.isDisplayed(), true);
	}

	@Test(description = "Swipe down and Tap scenario",enabled = false)
	public static void Test_05() throws Exception {
		AndroidDriver<AndroidElement> driver = getDriver();
		// boolean c = driver.isAppInstalled("com.eventyay.attendee");
		FooterOptions footer = new FooterOptions(driver);
		footer.getSearchIcon().click();
		SearchPage search = new SearchPage(driver);
		search.eventType.click();
		Common.swipeAndClick(driver, "Camp, Treat & Retreat");
		Assert.assertEquals(search.eventType.getText(), "Camp, Treat & Retreat", "Selected event is not visible");

		// Tap functionality.
		Common.tapOnElement(driver, search.searchArrow);
		Common.waitforPageToLoad(driver, 20);
		Common.tapOnElement(driver, search.filterIcon);
		Common.tapOnElement(driver, search.freeStuffCheckBox);
		System.out.println(search.freeStuffCheckBox.isSelected()+"::::::::::::::");
	}
	
	@Test(description = "Swipe down and Tap scenario")
	public static void Test_06() throws Exception {
		AndroidDriver<AndroidElement> driver = getDriver();



	}

}
